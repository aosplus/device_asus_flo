# Release name
PRODUCT_RELEASE_NAME := flo

# Boot animation
TARGET_SCREEN_HEIGHT := 1920
TARGET_SCREEN_WIDTH := 1200

# Inherit some common AOSPlus stuff.
$(call inherit-product, vendor/aosplus/config/common_full_tablet_wifionly.mk)

# Enhanced NFC
$(call inherit-product, vendor/aosplus/config/nfc_enhanced.mk)

# Inherit device configuration
$(call inherit-product, device/asus/flo/aosp_flo.mk)

## Device identifier. This must come after all inclusions
PRODUCT_DEVICE := flo
PRODUCT_NAME := aosp_flo
PRODUCT_BRAND := Google
PRODUCT_MODEL := Nexus 7 Flo
PRODUCT_MANUFACTURER := ASUS
PRODUCT_RESTRICT_VENDOR_FILES := false

